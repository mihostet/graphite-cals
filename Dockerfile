FROM brutasse/graphite-api

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y git openjdk-8-jdk

RUN pip install git+https://github.com/brutasse/graphite-api.git
RUN pip install cmmnbuild-dep-manager JPype1
RUN pip install git+https://github.com/michi42/pytimber
RUN pip install git+https://gitlab.cern.ch/mihostet/graphite-cals.git

RUN pip install gevent

RUN python -c 'import pytimber; cals=pytimber.LoggingDB(); print("pytimber import succeeded");'
RUN python -m cmmnbuild_dep_manager resolve

CMD exec gunicorn -b 0.0.0.0:8000 -w 10 -k gevent --timeout 300 --log-level debug graphite_api.app:app
