from graphite_cals import cals
from graphite_api.storage import FindQuery

def test_basic_data_extraction():
    start_time = 1503906147
    end_time = 1503992546
    cr = cals.CalsReader('CMS:LUMI_TOT_INST')
    time_info, data = cr.fetch(start_time, end_time)
    assert abs(time_info[0] - start_time) < 60
    assert abs(time_info[1] - end_time) < 60
    assert time_info[2] < 10


def test_scaled_data_extraction():
    start_time = 1503906147
    end_time = 1503992546
    cr = cals.CalsReader('CMS:LUMI_TOT_INST{1m}')
    time_info, data = cr.fetch(start_time, end_time)
    assert abs(time_info[0] - start_time) < 100
    assert abs(time_info[1] - end_time) < 100
    assert abs(time_info[2] - 60) < 2

def test_vectornumeric_extraction():
    start_time = 1503992546
    end_time = start_time + 200
    cr = cals.CalsFinder({}).find_nodes(FindQuery('CMS:BUNCH_LUMI_INST', start_time, end_time))
    all_series = list(cr)
    assert len(all_series) == 3564

def test_vectornumeric_extraction_for_variable_with_missing_metadata():
    start_time = 1503992546
    end_time = start_time + 200
    cr = cals.CalsFinder({}).find_nodes(FindQuery('XBH2.XDWC.021.526:PROFILE', start_time, end_time))
    all_series = list(cr)
    assert len(all_series) == 101

def test_empty_data_does_not_throw():
    start_time = 1504375786
    end_time = start_time + 200
    cr = cals.CalsReader('ATLAS:LUMI_TOT_INST')
    time_info, data = cr.fetch(start_time, end_time)
    assert data == []

def test_one_single_point_extraction():
    start_time = 1504519000
    end_time = 1504525500
    cr = cals.CalsReader('LHC.BLM.LIFETIME:B1_BEAM_LIFETIME')
    time_info, data = cr.fetch(start_time, end_time)
    assert data == [0.0]

def test_variable_search():
    start_time = 1503992546
    end_time = start_time + 200
    cr = cals.CalsFinder({}).find_nodes(FindQuery('%:LUMI_TOT_INST', start_time, end_time))
    all_series = list(cr)
    assert len(all_series) > 3


if __name__ == "__main__":
    test_basic_data_extraction()
    test_scaled_data_extraction()
    test_vectornumeric_extraction()
    test_vectornumeric_extraction_for_variable_with_missing_metadata()
    test_empty_data_does_not_throw()
    test_one_single_point_extraction()
    test_variable_search()

