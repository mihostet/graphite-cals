#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools

setuptools.setup(
    name='graphite-cals',
    version='0.0.18',
    description='Graphite-API to CALS bridge',
    author='Michi Hostettler',
    author_email='michi.hostettler@cern.ch',
    url='https://gitlab.cern.ch/mihostet/graphite-cals',
    packages=['graphite_cals'],
    install_requires=[
        'matplotlib==2.2.2',
        'python-dateutil',
        'pytz',
        'graphite-api',
        'pytimber',
        'numpy'
    ]
)