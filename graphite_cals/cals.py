from graphite_api.node import LeafNode, BranchNode
from graphite_api.intervals import IntervalSet, Interval
from datetime import timedelta
import numpy as np
import time, re
import pytimber
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("graphite_cals")

POINTLIMIT = 1e6
MAX_TIME_PERIOD_DAYS = 10
cals = pytimber.LoggingDB(appid='OP_GRAFANA', clientid='OP_GRAFANA')


def get_vectornumeric_size(var):
    size = var.getVectornumericSize()
    if size is None:
        variable = var.getVariableName()
        log.info("variable %s has no vectornumeric size, guessing from latest point ..." % variable)
        return cals.get(variable, time.time(), 'last')[variable][1].shape[1]
    else:
        return size.longValue()


def parse_timedelta(time_str):
    if time_str is None:
        return None
    match = re.match('((?P<days>\d+?)d)?((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?', time_str)
    params = {n: int(p) for n, p in match.groupdict().items() if p is not None}
    delta = timedelta(**params)
    if delta.total_seconds() < 1:
        raise ValueError("Invalid time scaling interval: %s" % time_str)
    return delta


def parse_query(path):
    match = re.match('(?P<variable>[^\{\}]*)(?P<raw_options>\{(?P<options>.*)\})?', path).groupdict()
    if match['raw_options'] is None:
        match['raw_options'] = ''
    return match


def fetch_cals_data(path, start_time, end_time):
    query = parse_query(path)
    options = query['options']
    variable = query['variable']
    time_scale = parse_timedelta(options)
    if (end_time - start_time) > (MAX_TIME_PERIOD_DAYS * 3600 * 24):
        raise ValueError("the requested time period is longer than the max. authorized: %.1f days"%(MAX_TIME_PERIOD_DAYS))
    log.info("fetch %s between %d and %d - time scale %s" % (variable, start_time, end_time, str(time_scale)))
    if time_scale is None:
        ts, data = cals.get(variable, start_time, end_time)[variable]
    else:
        ts, data = cals.getScaled(variable, start_time, end_time,
                                  scaleAlgorithm='REPEAT', scaleInterval='SECOND',
                                  scaleSize=('%d' % time_scale.total_seconds()))[variable]
    log.info("data downloaded.")
    if len(ts) == 0:
        return (start_time, end_time, end_time - start_time), np.array([])
    elif len(ts) == 1:
        return (ts[0], ts[0] + 1, 1), data

    step = max(int(np.min(np.diff(ts))), 1)
    if (end_time - start_time) / step > POINTLIMIT:
        step = max(int((end_time - start_time) / POINTLIMIT), 1)

    log.info("step = %d, interpolating ..." % step)
    time_interp = np.arange(int(ts[0]), int(ts[-1]), step)
    if data.ndim == 1:
        data_interp = np.interp(time_interp, ts, data)
    elif data.ndim == 2:
        data_interp = np.apply_along_axis(lambda x: np.interp(time_interp, ts, x), 0, data)
    else:
        raise ValueError("I only support 1D and 2D data, but not %d-D" % data.ndim)
    log.info("interpolation done")

    time_info = int(ts[0]), int(ts[-1]), step
    return time_info, data_interp


class CalsReader(object):
    __slots__ = ('path',)

    def __init__(self, path):
        self.path = path

    def fetch(self, start_time, end_time, *args):
        time_info, data = fetch_cals_data(self.path, start_time, end_time)
        return time_info, data.tolist()

    def get_intervals(self):
        return IntervalSet([Interval(0, time.time())])


class CalsVectorNumericReader(object):
    __slots__ = ('path', 'fetched_data')

    def __init__(self, path):
        self.path = path
        self.fetched_data = {}

    def fetch(self, start_time, end_time, index):
        try:
            time_info, data = self.fetched_data[(start_time, end_time)]
        except KeyError:
            log.info("data not yet downloaded from CALS")
            time_info, data = fetch_cals_data(self.path, start_time, end_time)
            self.fetched_data[(start_time, end_time)] = (time_info, data)

        return time_info, data[:, index].tolist()

    def get_intervals(self):
        return IntervalSet([Interval(0, time.time())])


class CalsVectorNumericLeafNode(LeafNode):
    __slots__ = ('index',)

    def __init__(self, name, reader, index):
        super(CalsVectorNumericLeafNode, self).__init__(name + ('[%04d]' % index), reader)
        self.index = index

    def fetch(self, start_time, end_time, *args):
        return self.reader.fetch(start_time, end_time, self.index)


class CalsFinder(object):
    def __init__(self, config):
        pass

    def find_nodes(self, query):
        log.info("find_nodes: " + str(query))
        request = parse_query(query.pattern)
        variables = cals.getVariables(request['variable'])
        log.info("found as many as %d variables" % (len(variables)))
        for var in variables:
            name = var.getVariableName()
            if str(var.getVariableDataType()) == 'VECTORNUMERIC':
                size = get_vectornumeric_size(var)
                reader = CalsVectorNumericReader(name + request['raw_options'])
                log.info("variable '%s' is VectorNumeric, returning %d series!" % (name, size))
                for index in range(0, size):
                    yield CalsVectorNumericLeafNode(name + request['raw_options'], reader, index)
            else:
                yield LeafNode(name + request['raw_options'], CalsReader(name + request['raw_options']))
